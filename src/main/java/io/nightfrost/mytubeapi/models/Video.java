package io.nightfrost.mytubeapi.models;

import java.lang.reflect.Field;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@Proxy(lazy = false)
public class Video {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(unique = true)
	private String name;

	@Lob
	private byte[] data;
	
	/*
	 * One Video has Many Comments
	 * NOT owning side.
	 */
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "video", cascade = CascadeType.ALL)
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Set<Comment> comments;
	
	@ManyToOne()
    @JoinColumn(name = "playlist_id", referencedColumnName = "id")
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private Playlist playlist;
	
	@ManyToOne()
    @JoinColumn(name = "user_id", referencedColumnName = "id")
	@EqualsAndHashCode.Exclude
	@ToString.Exclude
	private User user;

	public Video(String name, byte[] bytes, User userId) {
		this.name = name;
		this.data = bytes;
		this.user = userId;
	}
	
	/**
	 * @return Returns true or false, depending on if ALL fields hold data.
	 */
	public boolean isEmpty() {
		for (Field field : this.getClass().getDeclaredFields()) {
			try {
				field.setAccessible(true);
				if (field.get(this) != null) {
					return false;
				}
			} catch (Exception e) {
				System.out.println("Exception occured in validating fields..");
				System.out.println(e.getMessage());
			}
		}
		return true;
	}
}
